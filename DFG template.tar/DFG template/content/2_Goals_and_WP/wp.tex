%\fbox{\begin{minipage}{\textwidth}
%\underline{DFG Instructions}\\ 
%\textit{Please give a detailed account of the steps planned during the proposed funding period. (For experimental projects, a schedule detailing all planned experiments should be provided.)
%The quality of the work programme is critical to the success of a funding proposal. The work programme should clearly state how much funding will be requested, why the funds are needed, and how they will be used, providing details on individual items where applicable.
%Please provide a detailed description of the methods that you plan to use in the project: What methods are already available? What methods need to be developed? What assistance is needed from outside your own group/institute?
%Please list all cited publications pertaining to the description of your work programme in your bibliography under section 3.}\end{minipage}}

Below we outline specific work packages (WPs) which are suitable for two doctoral researchers and several Bachelor and Master students for a three-year period (requested funding duration).
Figure~\ref{fig.gantt} shows the time line for the work packages for the three-year project.

\begin{figure}[htb]
\begin{center}
\resizebox{\linewidth}{!}
{
\begin{gantt}[xunitlength=1.5cm,fontsize=\small,titlefontsize=\small,drawledgerline=true]{7}{9}
  \begin{ganttitle}
     \ganttlabel{Year}{}
    \titleelement{1st}{3}
     \titleelement{2nd}{3}
      \titleelement{3rd}{3}
  \end{ganttitle}
  \begin{ganttitle}
   \ganttlabel{Project}{month}
    \titleelement{1-4}{1}
    \titleelement{5-8}{1}
    \titleelement{9-12}{1}
    \titleelement{13-16}{1}
    \titleelement{17-20}{1}
    \titleelement{21-24}{1}
    \titleelement{25-28}{1}
    \titleelement{29-32}{1}
    \titleelement{33-36}{1}
  \end{ganttitle}
  \ganttbar[color=blue!20]{WP1 (Leader: Narayanaswamy)}{0}{4}
  %\addtocounter{ganttnum}{1}
  \ganttbarcon[color=blue!20]{WP2 (Leader: Park)}{1}{4}
  \ganttbarcon[color=blue!20]{WP3 (Leader: Narayanaswamy)}{3}{4}
  \ganttbarcon[color=blue!20]{WP4 (Leader: Steinhorst)}{4}{3.5}
  \ganttbarcon[color=blue!20]{WP5 (Narayanaswamy \& Park \& Steinhorst)}{4}{5}
% \ganttsync{1}{1.9}{1}{4}
%  \ganttsync{3}{3.9}{3}{7}
\end{gantt}}
\end{center}
\vspace{-0.7cm}
\caption{Time structure of work packages.}
\label{fig.gantt}
\end{figure}

\subsubsection{WP 1: Reconfigurable Energy Storage Element}
\label{subsubsec:WP1}
In this work package, we will develop an energy-efficient, reconfigurable energy storage element that can be dynamically programmed in order to achieve optimal results for different values of balancing currents. 

\textbf{WP 1.1: Inductor-array}\\
The existing inductor-based active cell balancing architectures \cite{kutkut}, \cite{DAC}, \cite{MartinCODES} etc., use a fixed value inductor that is optimized for a specific value of balancing current.
However, this equalization current value will change over time due to several environmental factors. 
For instance, batteries in \ac{EV} experiences a number of distinguishing usage scenarios.
If we compare two scenarios where an \ac{EV} battery pack is being charged with a fast DC charger within 30 minutes and being charged overnight using a slow level-1 charger for a period of 5 hours, the cell balancing current should be 10 times higher for the former.
It is well known that the energy efficiency of a switching converter, in this case the cell balancing circuitry, is a strong function of the load current.
As the amount of cell balancing current varies significantly depending on the timing requirements of different application scenarios, the energy efficiency also varies a lot.
%For instance, a higher value of balancing current is required during fast charging scenarios, whereas, the balancing could be performed with a reduced current value if the vehicle is charged over night. 
In such cases, architectures having fixed inductor that is optimized for a specific value of balancing current will provide non-optimal results for different values of balancing currents. 
In this part of the work package, we will develop novel methods of designing a variable inductor-array that can be dynamically reconfigured to support optimal balancing with different current values.
Our prime design objective in this task would be to obtain a reconfigurable inductor that is light in weight and occupies a reduced installation area favorable for implementation towards range critical applications such as \acp{EV}, \acp{HEV} etc.
To start with we will initially compare the system-level performance improvements obtained with multiple individual inductors that are optimized for different values of balancing currents as shown in Fig.~\ref{fig:inductorarray}a.
In the second step, we will propose a reconfigurable inductive potentiometer ($L^r$) as shown in Fig.~\ref{fig:inductorarray}b that can be programmed at real time to provide optimal results for different values of balancing currents.

\begin{figure}[t!]
\centering
\includegraphics[scale=0.4]{content/figures/inductorarray.pdf}
\caption{Reconfigurable energy storage element. (a) Multiple inductors each optimized for a specific value of balancing current. (b) Reconfigurable inductive potentiometer with multiple taps for optimal balancing at different equalization currents.}
\label{fig:inductorarray}
\end{figure}


\textbf{WP 1.2: Analytical model}\\
Our existing closed-form, analytical models for inductor-based active cell balancing architectures derived in \cite{DAC}, \cite{DATE} are based on a fixed inductance value.
These models cannot be directly applied to analyze the behavior of the reconfigurable inductor-array designed in \textbf{WP 1.1} of this project.
Therefore, in this task, we will extend our existing closed-form, analytical models derived for fixed configuration inductor-based active cell balancing architectures to support the reconfigurable energy storage element.
Such modified analytical models will enable us to develop several optimization frameworks for improving the energy efficiency and simulation algorithms for analyzing the system-level performance of our proposed approaches. 

\textbf{WP 1.3: Optimal range of inductance}\\
With the updated analytical models of our proposed reconfigurable energy storage element derived in \textbf{WP 1.2}, in this part of the work package, we will propose optimization frameworks for identifying the optimal inductance value for different values of balancing currents.
The value of inductance determines the energy efficiency and the installation area of an active cell balancing architecture. 
Too high a value of inductance will increase the installation area and weight since it requires an increased number of windings and a very low value of inductance will eventually increase the frequency of operation leading to high switching losses.
In addition, the minimum and maximum range of inductance values for the reconfigurable inductor-array designed in \textbf{WP 1.1} is also an important factor to be determined for efficient operation.
This is a challenging task, since this range depends upon different parameters such as the capacity of the battery pack, typical range of balancing currents required, \ac{SoC} distribution of cells in the pack, multiple charge transfer scenarios (cell-to-cell, many-to-many etc) that will be performed and so on. 
Finding an optimal range of inductance is imperative since this determines the installation area and the weight of our reconfigurable inductor-array, which has significant influence on the driving range obtained in an \ac{EV} or \ac{HEV}. 

\subsubsection{WP 2: Architecture Reconfiguration}
\label{subsubsec:WP2}

Apart from the reconfigurable energy storage element designed in Section~\ref{subsubsec:WP1}, in this work package, we will propose methods for reconfiguring the circuit configuration of the active cell balancing architecture depending upon the charge transfer scenarios. 

\textbf{WP 2.1: Combinations of existing architectures}\\
Existing inductor-based active cell balancing architectures are optimized for performing a particular type of charge transfer scenario.
For example, the architecture proposed in \cite{DAC} is capable of performing direct charge transfer between non-adjacent cells efficiently compared to the existing neighbor-only architectures in \cite{MKCC2013}, in which the excess charge is shuttled through all intermediate cells.
However, the architecture in \cite{DAC} will provide reduced energy efficiency in case of neighbor-only charge transfers compared to the architecture in \cite{kutkut}, which is optimized for such scenarios.
Similarly, the existing cell-to-cell charge transfer architectures such as \cite{MartinCODES}, \cite{NONB} are optimized for balancing between only single cells and their energy efficiency is less in comparison with \cite{ASPDAC} in case of many-to-many charge transfers for which the latter is optimized for. 
In this project, we will identify novel methods of combining such active cell balancing architectures, which are optimized individually for a specific type of charge transfer scenario in order to obtain an active equalization circuit that provides high energy efficiency for multiple charge transfer scenarios.

\begin{figure}[t!]
\centering
\includegraphics[scale=0.6]{content/figures/switchingnetwork.pdf}
\caption{Reconfigurable active cell balancing architecture consisting of programmable inductor-array and switching network.}
\label{fig:switchingnetwork}
\end{figure}

\textbf{WP 2.2: Reconfigurable switching network}\\
Existing inductor-based active cell balancing architectures consist of an energy storage element and a power \ac{MOSFET} switching network for routing the charge around cells.
The circuit connection configuration of the power \ac{MOSFET} switches in these architectures are fixed and is optimized for performing a particular type of charge transfer scenario. 
In contrast to this fixed circuit configuration of power \ac{MOSFET} switches, in this task, we propose a reconfigurable power \ac{MOSFET} switching network as shown in Fig.~\ref{fig:switchingnetwork} that can be dynamically programmed to perform multiple charge transfer scenarios.
For this purpose, we will propose smart real-time algorithms to find the optimal connection pattern of the reconfigurable switching network that will minimize the energy dissipation across the parasitic resistances and capacitances. 
Furthermore, we will propose a methodology to combine the reconfigurable inductor-array developed in Section~\ref{subsubsec:WP1} with the modular switching network to obtain a fully reconfigurable active cell balancing architecture that can efficiently perform equalization at different values of balancing currents and execute multiple charge scenarios.


\subsubsection{WP 3: Pack Topology Reconfiguration}
\label{subsubsec:WP3}

Here, we will propose a new reconfiguration architecture for the battery pack that has a reduced number of hardware components and still offers extensive reconfiguration capabilities compared to state-of-the-art approaches. 
Moreover, we will also integrate our proposed pack reconfiguration topology with the reconfigurable active cell balancing architecture developed in Sections~\ref{subsubsec:WP1} and \ref{subsubsec:WP2}, respectively. 

\textbf{WP 3.1: Pack reconfiguration architecture}\\
Reconfiguring the connection scheme of the individual cells to form a scalable battery pack is not new and has been extensively studied in the literature, see \cite{Shin1,Shin2,Shin3,Shin4,Shin5,Shin6,Reconfig1,Reconfig2,Reconfig3,Reconfig4}.
Each approach consists of certain number of switches that allow reconfiguration of the individual cells to form a flexible battery pack with unique characteristics. 
For instance, the reconfiguration architecture proposed in \cite{Reconfig1,Reconfig2} consists of 6 switches per cell as shown in Fig.~\ref{fig:reconfiguration}a and it allows the individual cells to be connected in series or parallel or in series/parallel combinations.
On the other hand, the reconfiguration topologies in \cite{Shin2}, \cite{Shin3} and \cite{Shin5} consist of only 3 switches per cell module as shown in Fig.~\ref{fig:reconfiguration}b and provides limited flexibility be allowing either series or parallel connection of individual cells. 
Moreover, topologies proposed in \cite{Reconfig3,Reconfig4} only has one switch in each module as shown in Fig.~\ref{fig:reconfiguration}c and allow to only isolate the cells from the main power line of the battery pack. 
In this work package, we will propose a novel reconfiguration architecture that has less number of hardware components compared to \cite{Shin1}, \cite{Shin4}, \cite{Shin6}, \cite{Reconfig1} and \cite{Reconfig2} and allows additional reconfiguration capabilities compared to \cite{Shin2,Shin3}, \cite{Shin5}, \cite{Reconfig3} and \cite{Reconfig4}. 

%
%For instance, the reconfiguration architecture proposed in \cite{Shin1} is shown in Fig.~\ref{fig:topology_reconfiguration} and it has 3 switches per cell module.
%With these architectures cells can either be connected in series or in parallel or can be isolated from the main power line of the battery pack as long as they are adjacent to each other.
%However, arbitrary connection of cells in series or parallel, for example connecting cells $B^1$ and $B^4$ in parallel and then connecting $B^2$ and $B^3$ in series to them is not possible.
%Therefore, in this project, we will propose a fully reconfigurable system architecture that provides a higher flexibility in connecting the arbitrary cells of the battery pack to realize any desired topology efficiently.
%%With this architecture it is possible to connect adjacent cells $B^1$, $B^2$ and so on in either series or parallel however, reconfiguring non-adjacent cells for example connecting $B^1$ and $B^4$ in either series or parallel and connecting cells $B^2$ or $B^3$ to this module is not possible. 

\begin{figure}[t!]
\centering
\includegraphics[scale=0.35]{content/figures/reconfiguration.pdf}
\caption{Reconfigurable active cell balancing architecture consisting of programmable inductor-array and switching network.}
\label{fig:reconfiguration}
\end{figure}

\textbf{WP 3.2: Combining reconfigurable active cell balancing and pack topology reconfiguration}\\
In this task, we are interested in obtaining a fully flexible, modular battery pack by efficiently combining both the reconfigurable active cell balancing architecture and the pack topology reconfiguration proposed in Sections~\ref{subsubsec:WP2} and \ref{subsubsec:WP3}, respectively.
This provides significant advantages in preforming active cell balancing.
For instance, the reconfigurable active cell balancing architectures proposed in Section~\ref{subsubsec:WP2} can optimally equalize at different values of balancing currents and perform multiple charge transfer scenarios and the pack topology reconfiguration architecture proposed in \textbf{WP 3.1} will allow flexible interconnections between arbitrary cells in the pack.
Combining these two approaches minimizes the parasitic resistances and capacitances of the circuit components in the current flow path, which increases the energy efficiency of the equalization process.
Moreover, with the flexibility offered by the pack topology reconfiguration, more interconnection paths are obtained that will increase the percentage of concurrent charge transfers between cells to minimize the overall equalization time.
Towards this perspective, in this task, we will propose efficient circuit architectures that combine both active cell balancing and pack topology reconfiguration methodologies presented in this project.
In addition, we will also propose system-level balancing strategies that will fully utilize the potentials of the underlying reconfiguration architectures in order to improve the usable capacity and lifetime of the battery pack. 


\subsubsection{WP 4: Prediction-based Management Algorithms}
\label{subsubsec:WP4}
In this project, we will propose smart battery management algorithms considering the environmental factors for efficiently using our active cell balancing and pack topology reconfiguration architectures.

\textbf{WP 4.1: When to balance?}\\
Even though active cell balancing improves the usable capacity of a series-connected battery pack, performing balancing all the time whenever there is an imbalance in \ac{SoC} of individual cells is also not beneficial.
This is due to the fact that each charge transfer activity will involve a certain amount of loss introduced by the parasitic resistances and capacitances of the circuit components in the current flow path.
Therefore, it is important to understand the system behavior and start the equalization process at an appropriate time instant which will ensure optimal usage of the battery pack.
For instance, a small variation in \ac{SoC} between cells can be tolerated if the intended driving range of the vehicle is achievable with the existing capacity of the battery pack and the equalization process can be initiated with the next charging activity to minimize the losses. 
On the other hand, if the intended driving range is not achievable with the current battery pack capacity, then the balancing can be started immediately after a certain threshold or it can be performed with a very high current towards the end of charge termination.
Both approaches will have varying impacts on the overall system efficiency and an optimal scenario has to be determined for initiating the balancing process. 
We will propose efficient equalization strategies that consider the capabilities of our underlying reconfiguration architectures as well as the present and future states of the system for identifying an optimal starting point of the balancing process. 


\textbf{WP 4.1: Prediction algorithms}\\
In this task, we will develop smart algorithms that keep track of the past balancing activities and the system state to find optimal current and strategies that will maximize the energy output of a battery pack. 
Moreover, we will also propose sophisticated learning algorithms that analyze the usage profiles of the battery pack and take smart balancing decisions to enhance the overall system efficiency. 
These smart algorithms will also provide additional information regarding the problems associated with other system units in the application.
For example, a balancing activity that is triggered many times by only a particular weak cell might be an indication that this cell is prone to malfunction earlier than other cells in the pack. 
This information might be useful during regular maintenance periods during which additional precautions could be taken to avoid premature failure of the entire battery pack.
Furthermore, such techniques can also provide the quality of homogeneity in the cells, which will assist in future battery pack designs.


\subsubsection{WP 5: Hardware Demonstrators and Simulation Frameworks}
\label{subsubsec:WP5}

In this work package, we will develop hardware demonstrator platforms and system-level simulation frameworks for analyzing the performances of our proposed reconfiguration architectures. 

\textbf{WP 5.1: Hardware demonstrators}\\
Individual hardware implementations of all our proposed reconfiguration architectures will be developed using commercial off-the-shelf components on a custom designed \ac{PCB}.
These hardware implementations stand as a proof-of-concept of our reconfiguration methodologies and enable us to verify the different claimed features of our proposed approach.
The design of the hardware implementations will be targeted to provide maximum flexibility in terms of functional evaluation and for taking high accuracy measurements.
For this reason, the hardware implementations will consist of monitoring circuitry for measuring all relevant parameters of the system and provide easy interface to external \ac{DAQ} systems such as LabVIEW or oscilloscopes to obtain high accuracy measurements for model validation purposes. 
In addition, we will also integrate these individual hardware prototypes in to our existing \ac{BMS} development platform in \cite{CPCSF} in order to verify the novel battery management algorithms that will be developed in Section~\ref{subsubsec:WP4} of this project. 

\textbf{WP 5.2: Simulation frameworks}\\
Verifying the system-level functionalities and evaluating performance of our proposed reconfiguration architectures based on hardware implementations require development of $96$ such prototypes in case of an \ac{EV} battery pack.
This will lead to an increased cost, design time, integration and management effort. 
Moreover, it is a challenging task to make real-time analysis of parameters from all $96$ prototypes in a laboratory setup.
Therefore, in this task of the project, we will develop fast and accurate simulation frameworks that takes into account the individual properties of the reconfigurable architectures and allow us to analyze the system-level performance of our proposed approaches.
Furthermore, such simulation frameworks will also allow us to propose several optimization approaches for improving the energy efficiency of the reconfigurable architectures. 
In addition, the simulation software will enable us to develop different smart battery management algorithms and efficiently characterize their performance on a battery pack level.











%\subsubsection{Work Package 1: Active Cell Balancing Circuit Architecture supporting Non-Adjacent and Concurrent Charge Transfer}\label{sec:cell_balancing}
%
%In this work package, we develop a circuit architecture and management technique for cell balancing that supports charge transfer among non-adjacent cells.
%
%\textbf{Task 1.1:} In this part of the work package, we develop the circuitry for supporting non-adjacent and concurrent charge transfers among cells.
%Figure~\ref{fig:cell_balancing} shows the conceptual diagram of the cell balancing circuit.
%While the idea of active cell balancing is not new~\cite{xx,xx}, very few works have proposed concrete architecture and actual implementations~\cite{xx}.
%In this work package, we will provide a concrete design and implementation of active cell balancing circuitry.
%To be more specific, the cell balancing circuitry will consist of multiple inductors and MOSFET switches to provide provide programmable and configurable charge transfer paths among cells.
%When it comes to non-adjacent cell balancing, it is important provide a direct path between them to avoid multi-hop charge transfers.
%%The energy efficiency of charge transfer among cells is inherently bound to the cycle efficiency of batteries.
%%For Lithium-ion cells, it is generally reported to be above 95\%, but it can be lower according to operating circumstances such as the operating temperature and current C rate.
%Our architecture will support this direct path between cells.
%Moreover, we plan to design a circuitry that supports concurrent charge transfers among different pairs of cells.

%\textbf{Task 1.2:} In this part of the work package, we develop strategies to balance the cells based on the cell balancing architecture proposed in Task 1.1.
%Fundamentally, it is an optimization problem with an objective of minimizing the energy loss during charge transfers under physical and timing constraints.
%The less energy wasted during cell balancing, the more energy used for supplying the load.
%The cells have to be balanced before the weakest cell reaches the lower threshold of SOC during discharge and the strongest cell reaches the higher threshold during charge.
%If the charging or discharging is being done at high C rates, cell balancing must be done accordingly faster.
%The concurrent charge transfers are constrained by shared MOSFET switches, which means the paths cannot overlap.
%Therefore, it is important to choose appropriate pairs of cells to allow more concurrency during cell balancing.
%Also, a pair of cells that are far away from each other is likely to suffer from transfer efficiency degradation as the path is longer.
%In Task 1.2, we model all such constraints and formulate an optimization problem to derive the optimal cell balancing strategy.

%\begin{figure}
%\centering
%\includegraphics[scale=1]{content/figures/cell_balancing.pdf}
%\caption{Active cell balancing circuit supporting charge transfer among non-adjacent cells.}
%\label{fig:cell_balancing}
%\end{figure}

%\subsubsection{Work Package 2: Reconfigurable Inductor Array for Dynamic Cell Balancing Current Adjustment}\label{sec:inductor_array}
%
%In this work package, we develop a reconfigurable inductor array that maximizes the charge transfer efficiency over a wide range of cell balancing currents.
%
%\textbf{Task 2.1:} In this part of the work package, we identify the specific requirement in cell balancing current according to different usage scenarios in different applications.
%For example, batteries in EV experiences a number of distinguishing usage scenarios.
%If we compared two scenarios where an EV battery is being charged with a fast DC charger within 30 minutes and it being charged within 5 hours, the cell balancing current should be 10 times higher for the former.
%It is well known that the energy efficiency of a switching converter, in this case the cell balancing circuitry, is a strong function of the load current.
%As the amount of cell balancing current varies significantly depending on the timing requirements of different application scenarios, the energy efficiency also varies a lot.
%In order to maximize the energy efficiency of charge transfer over a wide range of cell balancing current, we propose to use reconfigurable inductor array.
%Figure~\ref{fig:inductor_array} shows an abbreviated version of charge transfer circuitry between cells.
%\begin{figure}
%\centering\includegraphics[width=0.4\hsize]{content/figures/circuit}
%\caption{Reconfigurable inductor array for cell balancing circuitry.}\label{fig:inductor_array}
%\end{figure}
%Depending on the dynamically changing cell balancing current requirements, the proposed circuitry will be able to change the inductor values during runtime.
%We will consider multiple options such as multi-inductor array, inductive potentiometer, etc., in terms of energy efficiency and cost to find the most suitable option.
%
%\subsubsection{Work Package 3: Active Cell Balancing under Battery Pack Topology Reconfiguration}\label{sec:pack_topology}
%
%In this work package, we develop active cell balancing technique for an advanced battery architecture that supports pack topology reconfiguration.
%As the power and energy capacity requirement of batteries are increasing, the reliable operation of thousands of cells over a long period of time are becoming an issue.
%A recent work has proposed battery pack topology reconfiguration using MOSFET switches, which enables to bypass faulty cells in a large battery pack.
%This avoids a situation where failure of a single battery cell leading to failure of the total battery pack.
%Figure~\ref{fig:topology_reconfiguration} shows such topology reconfiguration circuit.

%If all series switches, $sw_{s,n}$, are closed and all parallel switches, $sw_{tp,n}$, and $sw_{bp,n}$ are open, all the batteries are in series connection.
%However, if the battery in the middle becomes faulty, it could be bypassed by opening $sw_{s,1}$ and closing $sw_{bp,1}$.
%As the demand for large capacity battery increases, such advanced architectures will be adopted for enhancing reliability.
%Hence, cell balancing techniques should be able to cooperate with such architectures.
%For example, severely degraded cells could either be excluded from the pack by bypassing or be balanced extensively by nearby cells.
%Whichever option is better depends on the specific scenarios and requires further investigation from this project.


%\subsubsection{Work Package 4: Combining the Work Packages 1-3 and Optimizing for Practical Applications and Scenarios}\label{sec:combination}
%
%In this work package, we combine the results of work packages 1 to 3 and optimize for practical applications and usage scenarios.
%
%\textbf{Task 4.1:}
%In this part of the work package, we combine the results of work packages 1, 2, and 3.
%The management techniques and circuit architectures proposed in work packages 1, 2, and 3 are actually a sub-problem for reaching the ultimate goal of designing a reliable and energy efficient cell balancing battery pack.
%As each sub-problem is solved independently to yield the circuit design and management techniques, it is important to combine the results while considering the synergy and redundancy in the merged designs.
%For example, the set of inductance values available for selection in work package 2 should be determined allowing for the cell balancing circuitry and technique from work package 1.
%The two problems will be intertwined as the cell balancing technique will affect the suitable inductance values and vice versa.
%The design space of the combined problem will be vast such that we will need efficient exploration techniques to search the optimal solution.
%
%\textbf{Task 4.2:}
%In this part of the work package, we further optimize the battery management circuitry as a whole.
%For example, cell balancing circuitry together with topology reconfiguration circuit would have an excessive number of \acp{MOSFET}, which will increase the overall cost of the battery pack.
%Instead of having a design that supports the most flexible cell balancing and topology reconfiguration, we will optimize the circuit while marginally compromising the energy efficiency and reliability.
%Figure~\ref{fig:temperature_distribution} shows the temperature distribution of a battery pack with air flow from left.
%The air warms up as it obtains heat from the battery cells such that the battery cells on the right constantly operate at a higher temperature.
%Uneven temperature distribution within a battery pack is very common.
%High operating temperature has detrimental effect on battery aging, which leads to accelerated aging of the cells on the right compared with the ones on the left.
%For such scenarios, it might make sense to provide a cell balancing path from cells on the far right to the cells on the far left rather than having the path among all cells.


%\begin{figure}
%\centering\includegraphics[width=0.7\hsize]{content/figures/temperature}\label{fig:temperature_distribution}
%\caption{Temperature distribution of a battery pack with air flow from left~\cite{pesaran1997thermal}.}
%\end{figure}
%
%%\subsubsection{Work Package 5: Usage Prediction-Based Active Cell Balancing}
%%
%%In this work package, 
%
%\begin{figure}
%\centering\includegraphics[width=0.7\hsize]{content/figures/usage_prediction}\label{fig:usage_profile}
%\caption{Exemplar usage profile of a EV battery.}
%\end{figure}
