\section{State of the art and preliminary work}
\label{section1}

In this section, we first provide a brief overview on the operating principle of an active charge transfer process. 
Next, we outline the existing active cell balancing architectures and the control algorithms that are available in the literature. 
Finally, we present our preliminary work in the domain of active cell balancing.

\subsection{Operating Principle}
\label{subsec:operatingprinciple}

\begin{figure}[t!]
\centering
\includegraphics[scale=0.4]{content/figures/architectureandcontrol.pdf}
\caption{Inductor-based active cell balancing architecture and its control signals. (a) Excess charge from cell $B^1$ is transferred to cell $B^2$ through inductor $L^1$. (b) Control signals required to perform the charge transfer and the corresponding balancing current through the inductor.}
\label{fig:working}
\end{figure}

The operation of an inductor-based active cell balancing architecture is based on the working principle of a buck-boost type DC-DC converter (for a general overview of a buck-boost DC-DC converter please refer to Chapter 6 in \cite{Switching}).
A state-of-the-art inductor-based active cell balancing architecture as proposed in \cite{kutkut} is shown in Fig.~\ref{fig:working}a.
Concurrent charge transfers between cells are possible with this architecture, however, the balancing is limited to only adjacent cells of the battery pack.
Each cell is associated with a balancing module that consists of two power \ac{MOSFET} switches ($M^1_a$ and $M^1_b$) and an energy storage element, inductor $L^1$.
Charge transfer between cells takes places in two phases, charging ($\Phi_1$) and discharging ($\Phi_2$) that are controlled by two high frequency control signals $\sigma^1$ and $\sigma^2$, respectively shown in Fig.~\ref{fig:working}b.


During the charging phase $\Phi_1$, \ac{MOSFET} $M^1_b$ is actuated with control signal $\sigma^1$ shown in Fig.~\ref{fig:working}b.
This charges the energy storage element inductor $L^1$ from cell $B^1$ and its current increases linearly from $0$ till a peak value $I_{\mathrm{peak}}$ as shown in Fig.~\ref{fig:working}b.
This maximum peak value of the balancing current is determined by the saturation current limit of the inductor and charging beyond this limit will significantly damage the inductor and the property of inductance is lost at which point it behaves like a wire short-circuiting the cell.
The discharging phase $\Phi_2$, during which the inductor discharges the stored energy to the cell $B^2$, is controlled by actuating \ac{MOSFET} $M^2_a$ with the control signal $\sigma^2$ shown in Fig.~\ref{fig:working}b.
Here, the inductor current decreases linearly from $I_{\mathrm{peak}}$ till $0$ as shown in Fig.~\ref{fig:working}b.
Short free-wheeling periods $\Phi'_2$, between the two phases ($\Phi_1$ \& $\Phi_2$), during which the balancing current flows through the internal body-diodes of the \acp{MOSFET} is necessary to prevent short circuit conditions.

\subsection{Related Work}
\label{subsec:relatedwork}

A comprehensive overview of existing balancing techniques is provided in \cite{review1,review2,review3,review4}.
Active cell balancing approaches are classified based on their type of energy storage elements and charge transfer scenarios. 


\subsubsection{Classification based on Energy Storage Element}
\label{subsubsec:energystorageelement}
Depending upon the type of energy storage element used to perform the charge transfer, active cell balancing architectures are classified as, capacitor-based, transformer-based and inductor-based architectures.

\minisection{Capacitor-based active cell balancing}
Capacitors are used as an energy storage element to transfer charge from overcharged cells to undercharged cells. 
In first step, the balancing capacitor is charged from the cell with high \ac{SoC}.
Once fully charged, in the second step, the balancing capacitor is connected across the cell with a low \ac{SoC}, in order to discharge the stored energy.
Simple control scheme and reduced installation area are the key benefits of this approach.
However, they can only achieve a maximum of \SI{50}{\percent} energy efficiency, due to the inherent energy dissipation involved in charging a capacitor directly from the battery cell.
Moreover, they also provide a reduced equalization speed in comparison to inductor-based or transformer-based approaches.
Capacitor based balancing technique can be implemented in three types as
\begin{itemize}
\item \textbf{Switched Capacitor \cite{SC1}}: Each cell is associated with a balancing capacitor and four \ac{MOSFET} switches that enable charge transfers between adjacent cells of the battery pack.
\item \textbf{Double-tiered switched capacitor \cite{DTSC1} and \cite{DTSC2}}: With two levels of capacitors involved, the double-tiered approach is capable of transferring charge between non-adjacent cells to reduce the balancing time compared to switched capacitor approach. 
\item \textbf{Single switched capacitor \cite{SSC1}}: In this method only a single capacitor is used as energy transfer element and each cell is accompanied with a switch network to connect and disconnect the cell with the capacitor, thereby enabling direct transfer of charge between non-adjacent cells.
\end{itemize}

\minisection{Transformer-based active cell balancing}
Transformer-based active cell balancing approaches work on the operating principle of flyback type DC-DC converters, see \cite{flybackDCDC} for more details.
Inherent galvanic isolation and fast equalization speed are the key advantages of transformer-based active cell balancing approaches.
However, their size and increased weight remain a challenge for modular implementation. 
Existing transformer-based approaches are classified into:
\begin{itemize}
\item \textbf{Single-winding transformer \cite{SWTR1} and \cite{SWTR2}}: This approach uses a single transformer as an energy storage element and employs a switching network to alternatively connect the cells that are required to be balanced.
\item \textbf{Multi-winding transformer \cite{MTWTR1}}: Here, faster equalization compared to the single-winding technique is achieved, where the primary winding of a custom-made transformer is connected to the individual cells and the secondary winding is connected across the battery pack. 
\textbf{\item Multiple transformers \cite{MTTR1}}: Each cell in the battery pack is associated with an individual transformer.
Primary winding of all transformers are connected to their respective cells and their secondary windings are connected to each other enabling charge redistribution between cells. 
\end{itemize}

\minisection{Inductor-based active cell balancing}
The working principle of inductor-based active cell balancing architectures is explained in Section~\ref{subsec:operatingprinciple}.
They are highly energy-efficient compared to the capacitor-based approaches, since they do not suffer from inherent \SI{50}{\percent} energy dissipation. 
Moreover, they occupy a reduced installation space compared to the transformer-based architectures, which is more favorable for range critical applications such as \acp{EV} and \acp{HEV}.	
Therefore, in this project, we focus on inductor-based active cell balancing architectures since they provide a higher energy efficiency compared to capacitor-based architectures and require a reduced installation area when compared to transformer-based approaches \cite{review2}.
Existing inductor-based balancing methods are broadly classified into:
\begin{itemize}
\item \textbf{Multiple inductors \cite{kutkut}}: Each balancing module consists of one inductor and two power \acp{MOSFET} as shown in Fig.~\ref{fig:working}a. 
Neighbor-only charge transfers reduces the efficiency of this approach when the imbalanced cells are farther away in the series-connection of the battery pack. 
\item \textbf{Single inductor \cite{SI1}}: All cells that are required to be equalized are connected to a single central inductor through a switching network. 
Even though this approach enables direct non-neighbor charge transfers, concurrent balancing is not possible which leads to longer equalization time.  
\item \textbf{DC-DC converters \cite{DCDC1} and \cite{DCDC2}}: Here well-known switched-mode DC-DC converter topologies such as buck-boost, Cuk, etc. are used for charge transfer between cells.
Increased control complexity is the prime disadvantage of these approaches, which make them less attractive for active cell balancing. 
\end{itemize}

\subsubsection{Classification based on Charge Transfer Scenarios}
\label{subsubsec:chargetransferscenarios}
Apart from categorizing active cell balancing architectures based on the type of energy storage element used, they are also classified based on their charge transfer scenarios as, Cell-to-cell, Cell-to-pack, Pack-to-cell and Cell to/from pack.
A good overview of these different charge transfer topologies is provided in \cite{Baronti} and \cite{Compare2}.

\minisection{Cell-to-cell}
Balancing architectures proposed in \cite{kutkut}, \cite{SC1} and \cite{DCDC1} fall under this charge transfer scenario.
Here, charge is transferred from a single source cell to a single destination cell in the battery pack.
According to \cite{Baronti}, this charge transfer scenario provides higher energy-efficiency than other topologies, since the difference in cell voltage between the input and output of the balancing architecture is small compared to other scenarios.


\minisection{Cell-to-pack}
Charge from a single source cell is transferred to a module of 8 to 12 series-connected cells. 
This method of charge transfer is beneficial during the charging process of the battery pack, where a single cell with higher \ac{SoC} can prematurely stop the charging process.
Therefore, quickly distributing its excess charge to other cells enables to fully charge the battery pack.
Balancing architectures such as in \cite{MKCC2013} and \cite{LT8584} fall under the cell to pack charge transfer scenario. 


\minisection{Pack-to-cell}
In the case of pack-to-cell charge transfer architectures, charge from a module of cells is transferred to a single cell. 
In contrast to the cell-to-pack scenarios, the pack-to-cell charge transfer methodology, can be typically used during the discharging process of the battery pack.
The discharging threshold is determined by the weak cell in the pack and by transferring charge from the entire module of cells to the weak cell, its \ac{SoC} is significantly increased.

\minisection{Cell to/from Pack}
This is the combination of both cell-to-pack and pack-to-cell charge transfer scenarios, where the charge can be transferred in both directions, from a single cell to the module of cell or vice versa. 
Balancing architectures proposed in \cite{LT} and \cite{Multiple} consist of individual bi-directional active cell balancing units, that can transfer charge from single cell to the module or vice versa. 


\subsection{Preliminary Work}
\label{subsec:preliminarywork}

In this section, we describe major preliminary work we have performed in the domain of active cell balancing.
Our proposed project mainly focuses on enhancing the capabilities of our previously published inductor-based active cell balancing architectures in \cite{DAC},\cite{MartinCODES}, \cite{SmartCell} and \cite{ASPDAC} (shown in Fig.~\ref{fig:architectures}) by introducing reconfiguration at different abstraction levels.
Fig.~\ref{fig:architectures}a shows a non-neighbor active cell balancing architecture proposed by us in \cite{DAC}.
This architecture is capable of performing direct charge transfers between non-adjacent cells of the battery pack. 
Each module consists of 10 high power \ac{MOSFET} switches and one inductor $L^1$.
\acp{MOSFET} $M^i_s$ and $M^i_p$ are high-power, cell isolation switches, which facilitates in isolating the intermediate cells of the battery pack while performing non-neighbor balancing. 
In contrast to the high-power isolation switches, which carry the high value battery pack currents, \acp{MOSFET} $M^i_e$, $M^i_f$, $M^i_l$ are low-power \ac{MOSFET} switches carrying only the low-value balancing currents. 
They are bi-directional switches that are connected in such a way that their internal parasitic body-diodes are blocking each other from conduction.
Charge transfer is accomplished by actuating \acp{MOSFET} $M^i_a$ and $M^i_b$ with high frequency control signals $\sigma^1$ and $\sigma^2$ shown in Fig.~\ref{fig:working}b.
Each cell or a group of parallel-connected cells of a high voltage battery pack is associated with such a homogeneous balancing unit and these individual units are interconnected to realize a system-level active cell balancing architecture. 
Detailed switching rules for performing multiple charge transfer scenarios using this architecture are explained in \cite{DAC}.


\begin{figure}[t!]
\centering
\includegraphics[scale=0.36]{content/figures/nonneighbor.pdf}
\caption{Proposed inductor-based active cell balancing architecture. (a) \cite{DAC}, (b) \cite{MartinCODES} and (c) \cite{ASPDAC}.}
\label{fig:architectures}
\end{figure}

Our proposed architecture in \cite{DAC} improves the energy efficiency compared to existing balancing architectures in \cite{kutkut} and \cite{MKCC2013} by enabling direct charge transfer between non-adjacent cells.
However, the high-power cell-isolation switches $M^i_s$ and $M^i_p$ increase the power dissipation during normal operation of the battery pack. 
Moreover, non-neighbor balancing using this architecture takes place by isolating the intermediate cells and as a result equalization cannot be performed while the pack is in operation. 
To overcome this, we proposed an inductor-based active cell balancing architecture in \cite{MartinCODES}, shown in Fig.~\ref{fig:architectures}b, without any high power \ac{MOSFET} switches in the main power-line of the battery pack.
Each homogeneous balancing unit in Fig.~\ref{fig:architectures}b consists of 12 low-power \acp{MOSFET} and one inductor $L^i$.
The energy storage inductor $L^i$ is connected to the cell through switch $M^i_l$.
\acp{MOSFET} $M^i_c$ and $M^i_d$ are bi-directional switches that are used to route the balancing current through the cells.
Charge transfer is facilitated by actuating \ac{MOSFET} switches $M^i_a$, $M^i_b$ and $M^i_e$ with high frequency control signals according to the switching scheme explained in \cite{MartinCODES}.
Bi-directional \ac{MOSFET} switch $M^i_f$ is used to establish the free-wheeling phases $\Phi'_2$ shown in Fig.~\ref{fig:working}.

In addition to the cell-to-cell charge transfer architectures, we have also proposed an active cell balancing architecture in \cite{ASPDAC}, see Fig.~\ref{fig:architectures}c that is primarily optimized to perform charge transfers between multiple source and destination cells.
Each unit of the proposed many-to-many active cell balancing architecture consists of $5$ \ac{MOSFET} switches and one inductor.
\ac{MOSFET} $M^i_l$ is used to connect the inductor to the battery cell and the charge transfer between cells is enabled by actuating switches $M^i_a$ and $M^i_b$ with non-overlapping high frequency control signals.
The motivation for development of this architecture emerges from the fact that the \ac{SoC} variation of cells in the battery pack is predominantly determined by the location of the cooling inlet in the battery pack. 
Cells that are farther away from the cooling inlet will have a reduced capacity compared to the cells that are nearer to the cooling inlet.
In such a scenario, transferring the excess charge from group of cells that are near the cooling inlet to group of cells farther away will increase both the energy efficiency and the speed of the equalization process as shown in \cite{ASPDAC}.

\minisection{Cell balancing circuit modeling}
In addition to the circuit architectures for performing efficient active cell balancing, we have also developed accurate closed-form analytical models, which enable us to analyze the system-level performance of our proposed architectures. 
For instance,  in \cite{DAC}, we have developed a second-order, closed-form analytical model for the cell-to-cell inductor-based active cell balancing architectures shown in Fig.~\ref{fig:architectures}a and \ref{fig:architectures}b.
Moreover, a many-to-many charge transfer model is proposed in \cite{ASPDAC} for analyzing the performance of the balancing architecture shown in Fig.~\ref{fig:architectures}c.
These analytical models enable us to calculate the energy efficiency of the charge transfer process considering the losses involved in the parasitic resistances and capacitances of the circuit components. 
In addition, using these detailed analytical models, we have proposed a fast and accurate system-level charge transfer simulation framework in \cite{TCADshort}.
Moreover, we have are also used these analytical models to perform optimization of the circuit components involved in the active cell balancing architecture and proposed several efficient system-level equalization strategies as will be explained in the following. 

\minisection{Cell balancing circuit design optimization}
Optimally dimensioning the components of an active cell balancing architecture is imperative to enhance its performance in terms of energy efficiency and installation area. 
For this purpose, we have proposed an efficient optimization framework in \cite{Indopt} for optimally dimensioning the energy storage element inductor in the active cell balancing architecture. 
Furthermore, our optimization approach proposed in \cite{DATE} performs an efficient design space exploration to find the optimal choice of inductors and \acp{MOSFET} in an active cell balancing architecture from a set of commercially available component choices.
Apart from the optimization framework, we have also worked on several system-level equalization algorithms, which focus on finding optimal charge transfer pairs of cells depending upon the \ac{SoC} distribution of the battery pack and the capabilities of the underlying active cell balancing architecture.

\minisection{Cell balancing algorithms}
For instance, we have proposed two equalization strategies in \cite{DAC} to perform fast and energy-efficient balancing using the non-neighbor architecture shown in Fig.~\ref{fig:architectures}a.
Moreover, in \cite{ASPDAC} a many-to-many equalization strategy is proposed for use with the multiple source to multiple destination cells charge transfer architecture shown in Fig.~\ref{fig:architectures}c.
Furthermore, four different equalization strategies are proposed in \cite{CPCSF} using our closed-form analytical models derived for the inductor-based active cell balancing architectures. 

\minisection{High-level battery management}
Design of a cell balancing circuit and its management should be done with regards to various usage scenarios such as Figure~\ref{fig:motivation}a and different battery applications.
In other words, preliminary works discussed above assume some sort of high-level battery management technique that defines the requirements and constraints for the cell balancing circuit design and management.
%We have performed relevant research in high-level battery management for various applications.
In~\cite{park:dac13}, battery management techniques for \ac{EV}s equipped with a supercapacitor array is proposed.
Battery management techniques for grid-connected battery storage are proposed in~\cite{wang:date13, wei:dac14, park:islped2012}.
These techniques aim at maximizing the energy efficiency or minimizing the electricity bill of a building equipped with solar panels.
The SOC and the load current changes from time to time, which in turn affects the constraints in cell balancing.

\minisection{Cell connection topology reconfiguration techniques}
In this project, we also consider pack topology reconfiguration in addition to cell balancing circuit reconfiguration.
We have devised a supercapacitor array reconfiguration technique to maximize the energy efficiency~\cite{kim:iccad2011}, which is very similar to battery pack reconfiguration.
Such a reconfiguration architecture could be adopted for battery pack topologies to bypass faulty cells, flexibly adapt to changing load requirements, etc.

The proposed project will build on our existing works on active cell balancing architectures.
Specifically, we will investigate the potential benefits obtained in terms of energy efficiency and equalization time by reconfiguring both the circuit components and the active cell balancing architectures itself to optimally perform multiple charge transfer scenarios.
For this purpose, we will extend our existing analytical model to include the effects of reconfiguration technology.
Furthermore, we are interested in combining our reconfigurable active cell balancing architecture with the existing battery pack reconfiguration methodologies and analyze the performance improvements that can be achieved.
Finally, we will propose efficient equalization strategies that fully utilizes the potentials of our reconfigurable active cell balancing architectures.

\subsubsection{Project-related Publications}
\label{subsec:projectrelatedpublications}

\begin{itemize}
\item Matthias Kauer, Swaminathan Narayanaswamy, Sebastian Steinhorst, Martin Lukasiewycz and Samarjit Chakraborty: \textit{Modular System-level Architecture for Concurrent Cell Balancing}. In Proceedings of $50^{\mathrm{th}}$ Design Automation Conference (DAC), 2013.
\item Matthias Kauer, Swaminathan Narayanaswamy, Sebastian Steinhorst, Martin Lukasiewycz and Samarjit Chakraborty: \textit{Many-to-Many Active Cell Balancing Strategy Design}. In Proceedings of $20^{\mathrm{th}}$ Asia and South Pacific Design Automation Conference (ASPDAC), 2015.
\item Martin Lukasiewycz, Sebastian Steinhorst and Swaminathan Narayanaswamy: \textit{Verification of Balancing Architectures for Modular Batteries}. In Proceedings of Hardware/Software Codesign and System Synthesis (CODES+ISSS), 2014.
\item Swaminathan Narayanaswamy, Sebastian Steinhorst, Martin Lukasiewycz, Matthias Kauer and Samarjit Chakraborty: \textit{Optimal Dimensioning of Active Cell Balancing Architectures}. In Proceedings of Design, Automation \& Test in Europe (DATE), 2014.
\item Matthias Kauer, Swaminathan Narayanaswamy, Sebastian Steinhorst, Martin Lukasiewycz and Samarjit Chakraborty: \textit{Inductor Optimization for Active Cell Balancing Using Geometric Programming}. In Proceedings of Design, Automation \& Test in Europe (DATE), 2015.
\item Matthias Kauer, Swaminathan Narayanaswamy, Sebastian Steinhorst and Samarjit Chakraborty: \textit{Rapid Analysis of Active Cell Balancing Circuits}. In IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems (TCAD), 2016. To appear.
\item Sebastian Steinhorst, Matthias Kauer, Arne Meeuw, Swaminathan Narayanaswamy, Martin Lukasiewycz and Samarjit Chakraborty: \textit{Cyber-Physical Co-Simulation Framework for Smart Cells in Scalable Battery Packs}. In ACM Transactions of Design Automation of Electronic Systems (TODAES), article = 62, volume = 21, issue = 4, June 2016.
\item Sebastian Steinhorst, Zili Shao, Samarjit Chakraborty, Matthias Kauer, Shuai Li, Martin Lukasiewycz, Swaminathan Narayanaswamy, Muhammad Usman Rafique and Qixin Wang: \textit{Distributed Reconfigurable Battery System Management Architecture}. In Proceedings of $21^{\mathrm{st}}$ Asia and South Pacific Design Automation Conference (ASP-DAC), 2016.
\item Sebastian Steinhorst, Martin Lukasiewycz, Swaminathan Narayanaswamy, Matthias Kauer and Samarjit Chakraborty: \textit{Smart cells for Embedded Battery Management}. In Proceedings of International Conference on Cyber-Physical Systems, Networks and Applications (CPSNA), 2014.
\item Younghyun Kim, Sangyoung Park, Yanzhi Wang, Qing Xie, Naehyuck Chang, Massimo Poncino and Massoud Pedram: \textit{Balanced Reconfiguration of Storage Banks in a Hybrid Electrical Energy Storage System}. In Proceedings of 2011 IEEE/ACM International Conference on Computer-Aided Design (ICCAD), 2011.
\item Sangyoung Park, Younghyun Kim and Naehyuck Chang: \textit{Hybrid energy storage systems and battery management for electric vehicles}. In Proceedings of 50th ACM/EDAC/IEEE Design Automation Conference (DAC), 2013.
\item  Yanzhi Wang, Xue Lin, Massoud Pedram, Sangyoung Park and Naehyuck Chang: \textit{Optimal Control of a Grid-Connected Hybrid Electrical Energy Storage System for Homes}. in Proceedings of Design Automation and Test in Europe (DATE), 2013.
\item Tianshu Wei, Taeyoung Kim, Sangyoung Park, Qi Zhu, Sheldon X. D. Tan, Naehyuck Chang, Sadrul Ula, and Mehdi Maasoumy: \textit{Battery Management and Application for Energy-Efficient Buildings}. in Proceedings of the IEEE/ACM Design Automation Conference (DAC), 2014.
\item Sangyoung Park, Yanzhi Wang, Younghyun Kim, Naehyuck Chang and Massoud Pedram: \textit{Battery Management for Grid-connected PV Systems with a Battery}. in Proceedings of IEEE/ACM International Symposium on Low Power Electronics and Design (ISLPED), 2012.
\end{itemize}






















